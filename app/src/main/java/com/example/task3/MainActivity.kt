package com.example.task3

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(applicationContext, "App Created", Toast.LENGTH_SHORT).show();

    }

    override fun onRestart() {
        super.onRestart() //call to restart after onStop
        Toast.makeText(applicationContext, "App Restarted", Toast.LENGTH_SHORT).show();
    }

    override fun onStart() {
        super.onStart() //soon be visible
        Toast.makeText(applicationContext, "App Started", Toast.LENGTH_SHORT).show();
    }

    override fun onResume() {
        super.onResume() //visible
        Toast.makeText(applicationContext, "App Resume", Toast.LENGTH_SHORT).show();
    }

    override fun onPause() {
        super.onPause() //invisible
        Toast.makeText(applicationContext, "App Pause", Toast.LENGTH_SHORT).show();
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(applicationContext, "App Stop", Toast.LENGTH_SHORT).show();
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(applicationContext, "App Destroy", Toast.LENGTH_SHORT).show();
    }
}